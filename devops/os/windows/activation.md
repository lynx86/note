### Windows10激活
1. 找到C:\Windows\System32下面的cmd，右键【以管理员身份运行】
2. 执行以下命令
```bash
$ slmgr /ipk W269N-WFGWX-YVC9B-4J6C9-T83GX
$ slmgr /skms zh.us.to
$ slmgr /ato
```
3. 等待激活完成，显示激活结果提示框
4. 执行以下命令，确认激活已完成
```bash
$ slmgr /xpr
```

正常情况下，系统已被激活180天，到期后可重复以上操作。
