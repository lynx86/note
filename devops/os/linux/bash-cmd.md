## 磁盘内存
### 查看磁盘空间
``` bash
$ df -h

Filesystem      Size  Used Avail Use% Mounted on
/dev/vda1        50G   20G   27G  42% /
devtmpfs        911M     0  911M   0% /dev
tmpfs           920M   24K  920M   1% /dev/shm
tmpfs           920M  520K  920M   1% /run
``` 

### 查看文件夹大小
```bash
$ du -h --max-depth=1 /data

28K	/data/docs
132K	/data/ghost-source
164K	/data
```

