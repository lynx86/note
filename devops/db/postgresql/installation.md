
## 使用Docker安装

### 下载镜像
```bash
# docker pull postgresql:12
```

### 启动镜像
```bash
# docker run -v /usr/local/docker/postgres/data:/var/lib/postgresql/data -v /usr/local/docker/postgres/log:/var/log/postgresql -v /usr/local/docker/postgres/config:/etc/postgresql --restart=always --privileged=true --name postgres -e POSTGRES_PASSWORD=password -p 5432:5432 -d postgres:12
```

### 进入镜像并登陆
```bash
# docker exec -it postgres /bin/bash

// 进入容器
# psql -U postgres
psql (12.7 (Debian 12.7-1.pgdg100+1))
Type "help" for help.

// 创建用户（密码为test，单引号）
postgres=# create user test with password 'test';
CREATE ROLE

// 创建数据库test（Owner为刚创建的test用户）
postgres=# create database test owner test;
CREATE DATABASE

// 赋权
postgres=# grant all on database solar to solar;
GRANT
```

好了，可以使用test用户正常登录使用test数据库了。