
## 连接池满

长时间使用后，再次启动SpringBoot应用失败，发现以下错误：
```bash
FATAL: remaining connection slots are reserved for non-replication superuser connections
```

Postgresql默认的最大普通连接数是100，超过以后会拒绝访问。

### 治标方法

#### 1.登录docker

```bash
# docker exec -it postgres /bin/bash

// 进入容器
$ psql -U postgres
```

#### 2.查看当前连接状况
```sql
$ select datname,pid,application_name,state from pg_stat_activity;
```

可以发现有很多的连接处于Idle状态，连接数已经在100以上。

#### 3.关闭当前处于idle状态的连接
```sql
$ select pg_terminate_backend(pid) from pg_stat_activity; 
```

#### 4.修改postgreSQL的最大连接数

```bash
// 修改postgres的配置
# cd /var/lib/postgresql/data
# vi  postgresql.conf

// 修改max_connections 为 200
```