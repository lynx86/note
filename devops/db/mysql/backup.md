## 每日定时备份数据

### 1. 添加备份脚本

如果是在docker中，需要先进入docker。

```bash
$ docker exec -it mysql /bin/bash

// 进入docker内部（选择根目录或其它目录下创建）
$ vi backup.sh
```

备份脚本如下：
```shell
#!/bin/bash

#保存备份个数，备份31天数据
number=31
#备份保存路径
backup_dir=/root/mysqlbackup
#日期
dd=`date +%Y-%m-%d-%H-%M-%S`
#备份工具
tool=mysqldump
#用户名
username=root
#密码
password=xxx
#将要备份的数据库
database_name=xxx-mall

#如果文件夹不存在则创建
if [ ! -d $backup_dir ]; 
then     
    mkdir -p $backup_dir; 
fi

#简单写法  mysqldump -u root -p123456 users > /root/mysqlbackup/users-$filename.sql
$tool -u $username -p$password $database_name > $backup_dir/$database_name-$dd.sql

#写创建备份日志
echo "create $backup_dir/$database_name-$dd.dupm" >> $backup_dir/log.txt

#找出需要删除的备份
delfile=`ls -l -crt  $backup_dir/*.sql | awk '{print $9 }' | head -1`

#判断现在的备份数量是否大于$number
count=`ls -l -crt  $backup_dir/*.sql | awk '{print $9 }' | wc -l`

if [ $count -gt $number ]
then
  #删除最早生成的备份，只保留number数量的备份
  rm $delfile
  #写删除文件日志
  echo "delete $delfile" >> $backup_dir/log.txt
fi
```

### 2. 添加定时任务

首先创建备份的定时脚本: db-backup.cron
```shell

// 每天早上2点 后面是备份脚本的位置
0 2 * * * /root/mysql_backup_script.sh

// 如果使用的docker，则如下（使用上例中的备份脚本文件路径）
0 2 * * * docker exec mysql /backup.sh
```

然后将脚本添加到定时任务中
```bash
$ crontab db-backup.cron

$ crontab -l  // 确认是否添加成功
```