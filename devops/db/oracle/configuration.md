## 权限配置

### 创建视图VIEW权限不足问题

仅仅赋予创建view权限是不够的，还需要另外两个权限。

```sql
grant create view to A;
grant select any table to A;
grant select any dictionary to A;
```