
## 安装

主要参考[此篇博文](https://www.jianshu.com/p/203447e25ad5)

其它注意点有：
- Kibana使用nginx代理访问

### Kibana使用nginx代理访问

使用nginx反向代理一般都不会使用到根目录，而是一个二级目录，例如www.riverecho.cn/kibana。直接在nginx中这样设置，kibana不会正常显示。需要配置它的kibana.yml。

此文件在docker的以下位置：/usr/share/kibana/config/kibana.yml，添加以下内容：

```yaml
...

server.basePath: "/kibana"
```

之后重启，就可以正常访问kibana了。