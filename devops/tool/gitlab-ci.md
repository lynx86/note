## 启动

### Spring项目打包时，找不到依赖的模块

工程目录如下：
```javascript

root
  moduleA
    ...
    pom.xml
  moduleB
    pom.xml
  ...
  .gitlab-ci.yml
  ...
```

当模块moduleA打包依赖moduleB时，.gitlab-ci.yml可如下设置：
```yml

...

project-compile:
  image: maven:3-jdk-8
  stage: compile
  tags:
    - mvn-test
  only:
    - master
  script:
    - cd moduleB
    - mvn $MAVEN_OPTS clean install -Dmaven.test.skip=true
    - cd ../moduleA
    - mvn $MAVEN_OPTS clean package -Dmaven.test.skip=true
  artifacts:
    expire_in: 1 days
    paths:
      - targets/*.jar

...
```

另外，被依赖模块moduleB的pom.xml中，应该有如下设置：
```xml
...

<build>
  <plugins>
    <plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>
      <!-- 没有一下设置，打包时moduleA找不到 -->
      <configuration>
        <classifier>exec</classifier>
      </configuration>
    </plugin>
  </plugins>
</build>

...
```

