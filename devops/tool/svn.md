
## 查看

### 查看个人提交详情

```bash
# svn log -v | sed -n '/username/,/-$/ p'
```

### 提交xxx目录下的所有待提交文件

```bash
# svn commit /xxx/*/**/*
```
