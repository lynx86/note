
## 安装

### docker-compose
```bash
# curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# sudo chmod +x /usr/local/bin/docker-compose
# sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# docker-compose --version
```

## 启动

### Mysql

```bash
$ docker run -p 3306:3306 --restart always --name mysql \ 
             -v /etc/localtime:/etc/timezone:rw \
             -v /etc/localtime:/etc/localtime:rw \
             -v /data/mysql/log:/var/log/mysql \
             -v /data/mysql/conf.d:/etc/mysql/conf.d \
             -e MYSQL_ROOT_PASSWORD=123456 \
             -d mysql:5.7 \
             --character-set-server=utf8mb4 \
             --collation-server=utf8mb4_unicode_ci
```

启动后的远程登录配置
```bash
// 容器外
$ exec -it mysql bash

// 进入容器后
# mysql -uroot -p
mysql# use mysql;
mysql# select host,user,plugin from user;
mysql# alter user 'root'@'%' identified with mysql_native_password by 'root'; // 修改root的加密方式
```


### Stats

```bash
// 只返回当前状态
$ docker stats --no-stream

// 格式化输出内容（包括容器名）
$ docker stats --format "table{{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}"

// 删除所有未打标签的Image
$ docker rmi $(docker images -q -f dangling=true)
```

#### 删除所有未打标签Image
```bash
docker images|grep none|awk '{print $3}'|xargs docker rmi

// ps:
// docker images ：镜像列表
// |grep none : 筛选含 none 的行
// |awk '{print $3}' ：awk行处理器打印第三个参数（IMAGE ID）
// |xargs ： 参数列表转换
// docker rmi ： 镜像删除命令
```
