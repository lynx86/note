
# 概括
  
当前版本为 **V2**

框架计划使用Excel表格信息导入系统，自动生成前后端代码。目前后端代码自动生成功能已实现，前端代码待两三个应用功能成熟后再实现，目前手动编辑中。

# 配置

搞不1

# 前端

前端框架使用[Vue3](https://www.vue3js.cn/docs/zh/guide/introduction.html)实现，UI框架使用[element-plus](https://element-plus.gitee.io/#/zh-CN/component/installation)。

框架分为基础 + 业务两个部分，基础部分包括用户、权限等基础设置，业务部分可以横向扩展。扩展业务一般需要添加三部分内容：
- 路由Route
- 数据管理Store
- 页面View

## 添加路由

在router/index.js中，添加对应的route页面。
```javascript

...
import CustomPage from '@/views/custom/Custom';

const routes = [

  ...
  {
    ...
    path: '/custom', name: 'Custom', component: CustomPage
  }
]
```

## 添加数据管理

在store目录下，添加custom模块对应的数据管理文件（如custom.js)即可，填入内容：
```javascript

import baseStore from './base';
import deepClone from '@/utils/deepClone';

const state = deepClone(baseStore.state);


const actions = { ...baseStore.getCrudActions('/custom/custom'), };

const mutations = { ...baseStore.mutations };

const getters = { };

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
```

接口路径为/custom/custom，则页面中的CRUD默认操作路径已此为根路径，如：
```text
- POST /custom/custom        创建一个CUSTOM对象
- GET  /custom/custom        取得CUSTOM对象列表（List<CustomVo>）
- DELETE /custom/custom/:id  删除一个CUSTOM对象
- PUT  /custom/custom/:id    更新一个CUSTOM对象
- GET  /custom/custom/:id    取得一个CUSTOM对象的详情（CustomDetailVo）
```


## 添加界面

### 模板内容

一个标准的Crud界面，其模板内容所示：
```javascript

<template>
  <el-row :gutter='20' >
    <el-col :span='20'>
      <crud-filter :columns="filterColumns" :filter="filter" />
    </el-col>
    <el-col :span='4' style='text-align: right'>
      <el-button size='mini' type='primary' @click='handleEdit()'>新建</el-button>
    </el-col>
  </el-row>
  <crud-table 
    :items="items" :columns="tableColumns" :page="page" :operations="operations"
    @page-change="handlePageCurrentChange"
    @edit-item="handleEdit"
    @remove-item='handleRemove' />

  <el-dialog title='编辑' v-model='editing'>
    <crud-form 
      :columns="editColumns" :rules="rules" :item="editItem" :submiting="submiting"
      @submit='handleSubmit' />
  </el-dialog>
</template>
```

几个基础的crud组件，分工如下：
- crud-filter 负责字段筛选
- crud-table 负责列表呈现
- crud-form 负责详情显示和编辑更新

### 页面逻辑

Vue3中对页面逻辑做了抽象，提出了compositeAPI的概念。框架中对应的主要逻辑，都封装在了/composables目录下：
```text
- useCrud.js      // 主要的CRUD操作
  - useCrudEdit     // 负责编辑、更新和提交逻辑 
  - useCrudQuery    // 负责筛选和查询逻辑 
- useDict.js      // 字段数据的存取
```

使用Crud的基本逻辑如下：
```javascript

import { computed, onMounted, toRefs, reactive, watch } from 'vue';
import { useStore } from 'vuex';

import CrudTable from '@/components/crud/CrudTable';
import CrudForm from '@/components/crud/CrudForm';
import CrudFilter from '@/components/crud/CrudFilter';
import { useCrudEdit, useCrudQuery } from '@/composables/useCrud';
import { useDictQuery } from '@/composables/useDict';
import Constants from '@/Constants';

const Field = Constants.Field;

// 列表字段
const TABLE_COLUMNS = [
  { prop: 'field1', label: 'label1' },
  { prop: 'field2', label: 'label2' },
  ...
];

// 列表操作
const TABLE_OPERATIONS = [
  { label: '编辑', event: 'edit-item'},
  { label: '删除', type: 'danger', event: 'remove-item'},
];

const FIELD2_OPTIONS = [
  { value: 1, label: 'A' },
  { value: 0, label: 'B' }
];

// 编辑字段
const EDIT_COLUMNS = [
  { prop: 'field1', label: 'label1', type: Field.INPUT },
  { prop: 'field1', label: 'label1', type: Field.SELECT, options: FIELD2_OPTIONS },
  ...
];

const EDIT_RULES = {
  field1: [{ required: true, message: '请输入Field1', trigger: 'blur' }],
  ...
};

// 过滤字段
const FILTER_COLUMNS = [
  { prop: 'field1', label: 'label1', type: Field.INPUT },
];

export default {

  components: {
    'crud-table': CrudTable,
    'crud-form': CrudForm,
    'crud-filter': CrudFilter
  },

  setup() {

    const store = useStore();

    const customQuery = useCrudQuery('custom');
    const customEdit = useCrudEdit('custom', customQuery.fetchItems);

    // Mount以后，开始查询列表
    onMounted(() => customQuery.fetchItems);

    return {

      // TABLE定义
      items: customQuery.items,
      tableColumns: TABLE_COLUMNS,
      operations: TABLE_OPERATIONS,

      // 筛选&分页
      filter: customQuery.filter,
      page: customQuery.page,
      handlePageCurrentChange: : customQuery.handlePageCurrentChange,

      // 编辑对象
      rules: EDIT_RULES,
      editColumns: EDIT_COLUMNS,
      editing: customEdit.editing,
      editItem: customEdit.editItem,
      submiting: customEdit.submiting,
      handleEdit: customEdit.handleEdit,
      handleRemove: customEdit.handleRemove,
      handleSubmit: customEdit.handleSubmit
    }
  }
}

```
