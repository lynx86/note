
关于pdfjs的使用，不同场景和架构有些微的差别。如果是传统web项目，在返回的html中包括pdfjs相关文件及pdf文件路径即可。

在uniapp中，可以直接在前端工程中引入并使用。

1. 下载pdf.js

去[官网](https://mozilla.github.io/pdf.js/getting_started/#download)下载稳定版的pdfjs，考虑兼容性可以选择*Prebuilt (ES5-compatible)*版本。

在项目根目录的static目录下新建pdf目录，将下载好的zip文件解压到static/pdf目录下。

```bash
> static
  > fonts
    > ...
  > pdf
    > build
    > web
    LICENCE
```

2. 去除跨域限制

**Step1**: 修改viewer.js代码，将跨域相关的代码注释掉。
```javascript
  ...
    // if (origin !== viewerOrigin && protocol !== "blob:") {
    //   throw new Error("file origin does not match viewer's");
    // }
  ...
```

**Step2**: 将默认URL置为空
```javascript

...
var defaultOptions = {
  ...
  defaultUrl: {
    value: "",   // 置空
    kind: OptionKind.VIEWER
  },
}
...
```

3. 调用pdfjs

根据使用场景，在uniapp中打开pdf可以有两种方式。

**方法1**：当前页面使用webview打开
```javascript
<template>
  <webview :src="pdfUrl" />
</template>

<script>
  ...
  methods: {
    showPdf(fileUrl) {
      this.url = '/static/pdf/web/viewer.html?file=' + decodeURIComponent(url);
    }
  }
</script>
```

**方法2**：新打开一个窗口
```javascript
<template>
  <view>
    <!-- 当前页面内容 -->
  </view>
</template>

<script>
  ...
  mtods: {
    showPdf(fileUrl) {

      const url = '/static/pdf/web/viewer.html?file=' + decodeURIComponent(url);
      const view = plus.webview.create(url, 'id', {
				top: uni.getSystemInfoSync().statusBarHeight + 50,
				scalable: true, 
			}, { preload: 'preload webview' });
			const currentView = this.$mp.page.$getAppWebview();
			currentView.append(view);
    }
  }
</script>
```
