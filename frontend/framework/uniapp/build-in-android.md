
快速离线打包Android

## 准备工作

1. 下载AndroidStudio并准备相关SDK
2. 下载HBuilder并生成Uniapp项目（不要自动生成，到[开发者中心](https://dev.dcloud.net.cn/)手动生成新项目，后续要生成AppKey）

## 离线打包

### Step1：下载App离线SDK

从[官网](https://nativesupport.dcloud.net.cn/AppDocs/download/android)下载官方用于Android离线打包的SDK工程包。解压后，将HBuilder-Integrate-AS目录导入到AndroidSDK。


### Step2：生成AppKey

登录[开发者中心](https://dev.dcloud.net.cn/)，点击对应的App进入到【离线打包Key管理】。输入Android包名和Android证书签名SHA1，即可得到AppKey。

本地生成SHA1的命令如下（以应用名为myapp为例）：

```bash
$ keytool -genkey -alias myapp -keyalg RSA -keysize 2048 -validity 36500 -keystore myapp.keystore
```

** 注意 ** 生成的myapp.keystore请务必放到HBuilder-Integrate-AS/simpleDemo目录下。

### Step3：配置AppKey

在AndroidManifest.xml中，修改AppKey（注意不要修改dcloud_appkey）
```xml
...
<meta-data
  android:name="dcloud_appkey"
  android:value="Step2生成的AppKey" />
...
```

### Step4：修改生成签名配置

在simpleDemo/build.gradle文件中，修改两处内容。

```bash
...
android {
  ...
  defaultConfig {
    applicationId "cn.riverecho.test"       // 这里与Step2中填入的Android包名要一致
    ...
  }
  signingConfigs {
    config {
      keyAlias 'myapp'                   // Step2生成的keystore别名（-alias）
      keyPassword 'myapp'                // Step2提示输入的密码
      storeFile file('myapp.keystore')   // Step2中生成的keystore文件
      storePassword 'myapp'              // Step2提示输入的密码
      v1SigningEnabled true
      v2SigningEnabled true
    }
  }
  。。。
}
```

### Step5：修改AppID

修改assets/data/dcloud_control.xml，将uniapp的AppId替换。
```xml
<hbuilder>
<apps>
    <app appid="准备工作中生成项目的AppID" appver=""/>
</apps>
</hbuilder>
```

### Step6：生成离线打包资源

在HBuilder中运行【发行 -> 原生App-本地打包 -> 生成本地打包App资源】，生成成功后，将生成内容拷贝到Android项目的assets/apps目录下。

点击运行，一切OK。