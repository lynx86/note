
### H5中的路径参数

在Uniapp的页面跳转中，通常页面之间传递数据的方式是加上查询参数。例如

```javascript
uni.navigateTo({ url: 'pages/user/detail?type=admin' })
```

在detail.vue页面中，可以通过以下两种方式取得type参数：

```javascript
// 方式1：onload方法中的options参数
onload(options) {

  const type = options.type;
  ...
}

// 方式2：其它方法中通过this.$route.query获取
methods: {

  loadItems() {

    const type = this.$route.query.type;
    ...
  }
}
```

### APP或小程序的取法

以上方法对App或小程序无效，因为$route本身是使用了H5的规范实现的。

在App中可以通过Uniapp的其它API获取当前页面参数。

```javascript

function getCurPage() {
  const pages = getCurrentPages();
  return pages[pages.length - 1];
}

const curPage = getCurPage();
const queryParam = curPage.options || curPage.$route.query;

// 从queryParma中，即可获取前页面传递的数据

```