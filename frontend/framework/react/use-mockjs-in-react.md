
## 安装Mockjs

```bash
$ yarn add mockjs
```

## 编辑mock数据

在src目录下创建mock文件，如user.js来响应user相关请求。

```javascript

import Mock from 'mockjs';

const refreshMock = Mock.mock(/api\/user\?/, 'get', {
  errcode: 0,
  errmsg: '',
  data: {
     'items|10': [{
       'id|+1': 1,
       'name': '@cname',
       'name_en': '@first',
       'gender|1': ['0', '1'],
       'birthday': '@date',
       'email': '@email',
       'words': '@csentence'
     }],
     'total|200-300': 250
  }
});

export default refreshMock;
```

## 将mock文件导入到App中

编辑App.js（或其他React入口）

```javascript
import React from 'react';

...
import './mock/user';

...
function App() {
  ...
}

export default App;
```

## 使用axios发送请求

在业务代码中，使用axios或其它模块发送http请求，mockjs会自动拦截并响应mock数据。
```javascript
import React from 'react';
import axios from 'axios';

...

axios.request('/api/user').then(rsp => console.log(rsp));
...
```

## 注意事项

### mock匹配url最好使用正则表达式

在官方说明中，虽然也可以使用字符串来响应请求：
```javascript
const refreshMock = Mock.mock("api/user", 'get', {
  ...
}
```

但这样配置只能响应http://localhost:3000/api/user 这个请求，并且是严格匹配的。

一旦URL路径上有查询参数等，将不会被匹配上。例如请求http://localhost:3000/api/user?name=abc ，将返回404。

### mock匹配待查询参数的请求

对于正则表达式/api\/user/，会响应所有http://localhost:3000/api/user 的请求，当然也包括:

- http://localhost:3000/api/user/validate
- http://localhost:3000/api/user?name=abc

如果路径有重叠，将很可能不会返回用户期待的mock数据。因此，如果有两个请求路径分别是api/user和api/user/validate，那么对应的正则表达式最好写作：
- /api\/user\?/
- /api\/user/validate\?/

这两个参数都带上query字段，就可以区分开来。
