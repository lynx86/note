
## create-react-app添加alias

使用create-react-app创建react项目后，可以使用customize-cra工具修改webpack配置，其中alias功能修改如下：

修改config-overrides.js
```javascript
...
const path = require('path');
...

module.exports = override(
  ...
  addWebpackAlias({
    '@': path.resolve(__dirname, 'src')
  })
);

```
