
## 1.下载整个图标集

选择图标较少时,可以逐个加入到购物车中.

如果要下载整个图标集时,打开图标集页面,在开发者工具中执行以下脚本,可直接将所有图标加入购物车.
```javascript
var span = document.querySelectorAll('.icon-cover');
for (var i = 0, len = span.length; i < len; i++) {
     console.log(span[i].querySelector('span').click());
}
```

执行脚本完成后,在购物车中将会出现添加的所有图标.点击下载素材,则可以得到download.zip压缩文件包.

## 2.修改CSS引用路径

解压下载的zip文件,可以看到文件夹中包含以下文件:

- iconfont.css
- iconfont.eot
- iconfont.svg
- iconfont.ttf
- iconfont.woff
- iconfont.json

将所有文件拷贝到React项目的资源目录下,打开iconfont.css并修改,将相关文件的引用地址修改为相对路径.
```css
@font-face {font-family: "iconfont";
  src: url('/iconfont.eot?t=15685609421'); /* IE9 */   // 类似的路径都修改为 url('./xxxxx')
  ...
```

## 3.引入css文件

在React入口代码出,引入修改的css文件.
```javascript
import React from 'react';
...

import './assets/icon/iconfont.css';
...
```

## 4.使用icon

使用方式可以有两种,unicode方式或者直接class名方式.每个icon的信息可以在iconfont.json中找到.

```javascript
...
  <i class='iconfont'>&#x33;</i>     //  unicode方式引用
  <i class='iconfont icon-menu></i>  // class名方式引用
...
```
