
### create-react-app项目

```bash
$ create-react-app myApp
```

### 添加element-ui
#### 1.安装依赖
```bash
$ yarn add element-react
$ yarn add element-theme-default 
```
#### 2.加入样式
在app.js中引入element-ui的样式
```javascript
...
import 'element-theme-default';
...
```

### 添加mobx
#### 1.安装mobx依赖
```bash
$ yarn add mobx mobx-react
```

#### 2.安装customize-cra依赖

mobx使用@标注功能，此功能在ES7中尚属于experimental功能，creact-react-app官方暂不支持，因此需要引入babel进行转译。

但对于create-react-app项目，因为隐藏了webpack相关配置，如果想要自定义配置，需要使用react-app-rewired模块。在create-react-app版本2以后，官方建议搭载[customize-cra](https://github.com/arackaf/customize-cra#available-plugins)使用味道更好。

使用customize-cra后，不再需要babel模块。
```bash
yarn add create-react-app customize-cra --dev
```

#### 3.配置config-overrides.js
```javascript
const { 
    override,
    addDecoratorsLegacy,
    disableEsLint
} = require('customize-cra');

module.exports = override(

    addDecoratorsLegacy(),

    disableEsLint()
);
```

#### 4.版本说明

1. mobx-react在版本6时使用了react的钩子(hook)特性，钩子特性是在React版本16.8后引入的。
2. 在版本16.7以后，React已经弃用了componentWillReceiveProps和componentWillMount方法，这可能会影响到多个React第三方组件。
3. 为了兼容性考虑，可以讲React版本限定在16.4.1，同时将mobx-react的版本限定在5.x.x（最高5.4.4）