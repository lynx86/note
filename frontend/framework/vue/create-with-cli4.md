
包含特性
- Typescript
- Vuex
- Element-UI
- Sass

## 1.安装vue-cli
```bash
# yarn global add @vue/cli
# vue --version // 确认版本
```

## 2.初始化项目
```bash
# cd xxx/xxx // 跳转到要安装的项目目录
# vue create projectA // 创建项目
```

其中选择Manually select feature，并使用空格键标记要选择的feature。安装完成后，按照提示进入项目目录，并确认安装成功。
```bash
# cd projectA
# yarn serve
```

## 3.基础配置
### 3.1 配置端口
根目录下添加vue.config.js文件，并加入以下配置：
```javascript

const devPort = 9876;

module.exports = {
    lintOnSave: process.env.NODE_ENV === 'development',
    productionSourceMap: false,
    devServer: {
        port: devPort,
        open: true,
        overlay: {
            warnings: false,
            errors: true
        },
        progress: false
    },
    
}
```
保存文件后重新执行yarn serve，可看到项目在9876端口运行。

### 3.2 加入ElementUI
A.添加element-ui
```bash
# yarn add element-ui
```

B.main.js中加入对应模块和CSS
```typescript
import Vue from "vue";
...

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";

Vue.use(ElementUI, { size: 'medium' });

...
```

C.Home.vue中加入代码测试引入效果
```vue
<el-button type='primary'>Login</el-button>
```

### 3.3 引入scss
若需要在vue的<style>中引入对应的scss变量，需要安装style-resources-loader。
```bash
# yarn add --dev style-resources-loader
# yarn add --dev vue-cli-style-resources-loader
```

安装完成后，在vue.config.js中添加以下配置
```javascript
...
module.exports = {
    ...
    pluginOptions: {
        "style-resources-loader": {
            preProcessor: "scss",
            patterns: [
                path.resolve(__dirname, "src/assets/styles/variables.scss"),
            ],
        },
    },
};
```

完成后重启，variables.scss中的变量可以正常在vue文件的<style>中引用。
