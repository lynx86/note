
# App开发

## Uniapp

### 首页白屏汇总

#### 1.引入了Mock

使用mockjs后，在H5模式下使用正常，但会导致App打包后白屏


### 使用v-slot不生效

大约在2019年中的版本后，官方开始支持v-slot语法，但仍然有巨坑。

```javascript

// 父组件调用
<n-list :items="items" v-slot:default="{item}">
  <view>{{item.name}}</view>
</n-list>

// 父组件实现
<block v-for="item in items" :key="item.id">
  <slot v-bind:item="item"></slot>
</block>

```

上述代码在H5上完美运行，但在App上毫无效果。原因就在于这个block元素上！改为view以后，列表项可以正常显示。

```javascript

<view v-for="item in items" :key="item.id">
  <slot v-bind:item="item"></slot>
</view>

```

# Electron开发

## Vue模板开发

### 使用electron模块出现：Uncaught ReferenceError: __dirname is not defined

在vue.config.js中，添加以下内容：

```javascript

module.exports = {

  ...

  pluginOptions: {
    electronBuilder: {
      nodeIntegration: true
    }
  }
}
```