
# 概述

概要说明Demo所用到的主要技术组件和架构。

# 环境

本Demo的运行环境如下：

| 环境/软件 | 版本 | 
| -- | -- | 
| MacOS | 10.12.6++
| Java | 1.8.0
| Maven | 3.6.1
| Docker | Version 18.06.1-ce-mac73++
| SpringBoot | 2.1.4
| SpringCloud | Greenwich
| SpringCloud Alibaba | 2.1.0

# 目录

概要说明Demo的搭建过程

# 初始化项目

## 项目结构

```text
- Spring-Cloud-Learning

  - xxx模块
    - src
      - main
        - java
      - resources
        - application.properties
        - bootstrap.properties
  - xxy模块
    - ...

  - bin
    - nacos      // nacos可执行文件目录
      - bin
        - ...
  
  pom.xml        // 统一定义父项目的依赖包版本信息
```

对于项目下的每一个模块来说，pom.xml可以直接引用父模块的pom.xml，保证了相关依赖的版本统一。

## 公共模块

编辑 pom.xml，加入基础依赖包。
```xml

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>cn.riverecho.cloud</groupId>
  <artifactId>cloud</artifactId>
  <version>0.0.1</version>
  <packaging>pom</packaging>

  <name>cloud</name>
  <description>Demo Project For Spring Cloud</description>

  <modules>
    <module>auth</module>
  </modules>

  <dependencyManagement>
    <dependencies>

      <!-- Spring Boot 2.1.X -->
      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-dependencies</artifactId>
        <version>2.1.4.RELEASE</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-dependencies</artifactId>
        <version>Greenwich.RELEASE</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Spring Boot Alibaba -->
      <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-alibaba-dependencies</artifactId>
        <version>2.1.0.RELEASE</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

    </dependencies>
  </dependencyManagement>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
  </properties>

  <dependencies>

    <!-- 这里会随着demo的深入，一步步添加对应依赖 -->

  </dependencies>

</project>
```

# 配置中心 [Nacos]

关于Nacos的介绍可以参考[官网](https://nacos.io/zh-cn/docs/what-is-nacos.html)。

## 一、docker启动Nacos

```bash
// 拉取镜像
$ docker pull nacos/nacos-server:1.1.3

// 启动
$ docker run --name nacos -d -p 8848:8848 --privileged=true --restart=always -e JVM_XMS=512m -e JVM_XMX=2048m -e MODE=standalone -e PREFER_HOST_MODE=hostname -v /home/nacos/logs:/home/nacos/logs  nacos/nacos-server:1.1.3
```

启动Nacos后，可以直接打开浏览器，验证是否启动成功。
```text
地址：http://127.0.0.1:8848/nacos
账号：nacos
密码：nacos
```

## 二、添加Nacos的配置依赖
编辑根目录的pom.xml，加入以下依赖：
```xml
  ...
  <dependencies>

    <!--配置中心-->
    <dependency>
      <groupId>com.alibaba.cloud</groupId>
      <artifactId>spring-cloud-alibaba-nacos-config</artifactId>
    </dependency>

  </dependencies>
  ...
```

## 三、添加Auth服务

### 1.添加框架项目工程

现在添加demo的第一个业务模块Auth，使用Spring Initializr创建一个框架项目，在根目录下。
```properties
- Spring-Boot-Learning
  - auth   // 创建的项目
    ...

  - pom.xml
```

### 2.修改项目pom依赖

编辑auth/pom.xml，修改为以下内容：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>cn.riverecho</groupId>
	<artifactId>auth</artifactId>
	<version>0.0.1</version>
	<packaging>jar</packaging>

	<name>auth</name>
	<description>Demo project for Spring Boot</description>

	<parent>
		<groupId>cn.riverecho.cloud</groupId>
		<artifactId>cloud</artifactId>
		<version>0.0.1</version>
	</parent>
	
	<dependencies>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		
	</dependencies>

</project>
```

编辑根目录的pom.xml，添加auth模块定义
```xml
...
  <modules>
    <module>auth</module>
  </modules>
...
```

### 3.配置auth模块的nacos相关内容

```properties
# bootstrap.properties

spring.cloud.nacos.config.server-addr=127.0.0.1:8848   // 已启动nacos的默认监听地址:端口
spring.application.name=auth  // 当前服务程序的名称

```

这里配置spring.application.name的目的是，远端nacos需要识别并动态修改auth模块的配置，当然需要发送名字给nacos。

### 4.加入测试代码

对于auth模块，加入以下测试代码。

```java
package cn.riverecho.auth.api.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RefreshScope
public class AuthController {

  @Value("${useLocalCache:false}")
  private boolean useLocalCache;

  @GetMapping("")
  public boolean getAuth() {
    return useLocalCache;
  }
}
```

*说明*
1. 其中的@Value("${useLocalCache:false}")，通常情况下SpringBoot会从application.properties文件中读取。有了nacos，多个微服务项目的相同配置项，可以通过nacos系统来保持同一份，并且可以动态修改。
2. @RefreshScope标注的作用是，当远端nacos的配置属性修改后，auth模块能够被动态更新。

## 三、测试Nacos的动态配置

*[注] 首先确保nacos服务已启动*

### 1.启动Auth模块的application

### 2.访问 http://127.0.0.1/auth 接口

返回值为：
```text
false
```

### 3.调用nacos的api对配置进行修改

```bash

$ curl -X POST "http://localhost:8848/nacos/v1/cs/configs?dataId=auth.properties&group=DEFAULT_GROUP&content=useLocalCache=true"
```
这里的dataId=auth.properties中，auth就是auth模块中定义的application.name。

### 4.打开nacos管理端验证
发现在【配置管理 -> 配置列表】中，已经出现了一条记录。

![img](./nacos-1.png)

### 5.访问 http://127.0.0.1/auth 接口
返回值为：
```text
true
```

### 6.再次修改配置
点击上图记录中的*编辑*操作，将useLocalCache改为false，并点击【发布】，再次访问 http://127.0.0.1/auth 接口，返回值为：
```text
false
```

### 结论
说明通过nacos，auth模块应用的配置被动态更新了。

# 注册中心 [Nacos]

每一个微服务都是一个单体应用，互相之间的调用需要记住生产者服务的地址。这些地址信息不可能让每个微服务自己去管理，需要统一交由第三方服务来管理，这正是Nacos集成的一项功能。

## 一、添加注册中心配置依赖

编辑根目录的pom.xml，加入以下内容：
```xml
  ...

  <dependencies>

    ...

    <!--注册中心-->
    <dependency>
      <groupId>com.alibaba.cloud</groupId>
      <artifactId>spring-cloud-alibaba-nacos-discovery</artifactId>
    </dependency>

  </dependencies>
```

## 二、注册Auth服务

### 1. 修改配置属性

auth模块的bootstrap.properties中，加入以下内容
```properties
...
server.port=8091

...
spring.cloud.nacos.discovery.server-addr=127.0.0.1:8848
```

### 2.加入@EnableDiscoveryClient标注

在AuthApplication.java中，加入@EnableDiscoveryClient标注：
```java
...

@SpringBootApplication
@EnableDiscoveryClient   // 确保Auth应用能被nacos服务找到
public class AuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}

}
```

### 3.验证结果

重启auth服务，启动日志中有以下记录：
```log
INFO 96920 --- [           main] b.c.PropertySourceBootstrapConfiguration : Located property source: CompositePropertySource {name='NACOS', propertySources=[NacosPropertySource {name='auth.properties'}, NacosPropertySource {name='auth'}]}
```

打开nacos管理网页 http://127.0.0.1:8848/nacos ，在【服务管理 -> 服务列表】中，已经出现了auth服务的记录。
![img](./nacos-2.png)

## 三、加入Gateway服务

上面提到微服务间的互相调用，都是通过服务名来找到对方。对于服务A来讲，要想访问服务B的api接口，可以通过类似于 http://A/xxx 这样的形式。

### 1.添加Gateway工程

类似于Auth服务，使用SpringInitializr创建一个简单的SpringBoot应用，其pom.xml定义如下：
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>cn.riverecho</groupId>
	<artifactId>gateway</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>gateway</name>
	<description>Demo project for Spring Boot</description>

	<parent>
		<groupId>cn.riverecho.cloud</groupId>
		<artifactId>cloud</artifactId>
		<version>0.0.1</version>
	</parent>
	
	<dependencies>

    <!-- 注意：这里的依赖在稍后例子中将被删除 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		
	</dependencies>

</project>
```

编辑根目录的pom.xml，添加auth模块定义
```xml
...
  <modules>
    <module>auth</module>
    <module>gateway</module>
  </modules>
...
```

### 2.修改配置内容

修改gateway的配置内容，如下：
```properties
# bootstrap.properties
server.port=8090

spring.cloud.nacos.config.server-addr=127.0.0.1:8848
spring.cloud.nacos.discovery.server-addr=127.0.0.1:8848

spring.application.name=gateway
```

### 3.加入@EnableDiscoveryClient

在gateway服务的主Application中，加入@EnableDiscoveryClient，同auth服务。

### 4.验证结果

启动gateway服务成功后，在nacos管理界面中，会出现该服务的实例。

![img](./nacos-3.png)

## 四、Gateway服务调用Auth服务

*注： 当前gateway服务未加入SpringCloudGateway功能，只当做一般消费者服务来调用。*

### 1.使用RestTemplate

新建AuthController.java，加入以下内容：
```java
package cn.riverecho.gateway.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class AuthController {

  private final RestTemplate restTemplate;

  @Autowired
  public AuthController(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @GetMapping("/auth")
  public String getAuth() {
    return restTemplate.getForObject("http://auth/auth", String.class);
  }
}
```

- 服务之间的调用格式为 http://[服务名]/[请求URL]，所以例子中是请求了auth服务的/auth。

在Gateway服务的主Application中，加入对应Bean声明。
```
# GatewayApplication.java

...
public class GatewayApplication {

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  ...
}
```

### 2.测试1

启动Gateway服务后，浏览器中请求 http://127.0.0.1:8090/auth ，返回以下错误：
```text
...
There was an unexpected error (type=Internal Server Error, status=500).
I/O error on GET request for "http://auth/auth": auth; nested exception is java.net.UnknownHostException: auth
```

说明Gateway服务通过地址 http://auth/auth 对Auth服务进行访问时，并没有找到对应的服务。

### 3.加入@LoadBalanced

以上错误发生的原因在于，虽然nacos已经发现并注册了gateway和auth服务，但RestTemplate只是一个基础的Rest请求处理类，并没有被整合到SpringCloud的服务治理中，又如何能通过服务名找到对应服务呢？

解决的办法是，加入@LoadBalanced标注。

```java
# GatewayApplication.java

...
public class GatewayApplication {

  @LoadBalanced   // 集成Ribbon，负责负载均衡
  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  ...
}
```

### 4.再次测试

启动Gateway服务后，浏览器中请求 http://127.0.0.1:8090/auth ，返回：
```text
false
```
这里的结果，即是直接访问auth服务 http://127.0.0.1:8091/auth 是一样的。

# 网关路由 [Gateway]

接下来，将Gateway服务升级为网关服务，本Demo中使用了SpringCloudGateway。修改后，请求将经由gateway服务的路由功能转发到auth服务，并返回请求的结果。

## 一、加入配置依赖

修改gateway模块的pom.xml，加入对应依赖，并删除spring-boot-starter-web。

```xml
  ...
  <dependencies>

    <!-- 之前的spring-boot-starter-web需要删除 -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-gateway</artifactId>
 		</dependency>
		
	</dependencies>
```

## 二、修改属性配置

为了方便展示，将之前的bootstrap.properties改为yaml格式。
```yaml
server:
  port: 8090

spring:
  application:
    name: gateway

  cloud:
    nacos:
      config:
        server-addr: 127.0.0.1:8848
        file-extension: yml     # 注意加这一行，nacos才会正确读取识别
      discovery:
        server-addr: 127.0.0.1:8848
    
    # 新加入以下配置
    gateway:
      routes:
      - id: auth-service
        uri: lb://auth          # lb为负载均衡的服务路径，后面是服务名（类似于@LoadBalanced）
        predicates:
          - Path=/auth/**       # 访问路径
```

## 三、删除显式的请求处理代码

删除gateway服务中的主Application和Controller代码，直接使用配置的路由数据进行请求转发。

```java
# GatewayApplication.java

...
public class GatewayApplication {

  // 以下删除
  // @LoadBalanced   // 集成Ribbon，负责负载均衡
  // @Bean
  // public RestTemplate restTemplate() {
    //return new RestTemplate();
  //}

  ...
}
```

## 测试

启动Gateway服务，输入 http://127.0.0.1:8091/auth ，返回数据为：
```text
false
```

说明gateway的服务请求转发成功。

# 微服务间的请求调用 [Feign]

上一节内容中，用到了RestTemplate对其它微服务发送请求。而使用SpringCloud中的Feign模块，可以用更优雅的方式实现。

这一节的实例里会新加一个dept微服务，作为auth微服务的消费者，将向auth服务发送数据请求。

## 一、创建Dept服务

### 1.添加Dept工程

类似于Auth服务，使用SpringInitializr创建一个简单的SpringBoot应用，其pom.xml定义如下：
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>cn.riverecho</groupId>
	<artifactId>dept</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>dept</name>
	<description>Demo project for Spring Boot</description>

	<parent>
		<groupId>cn.riverecho.cloud</groupId>
		<artifactId>cloud</artifactId>
		<version>0.0.1</version>
	</parent>
	
	<dependencies>

    <!-- 注意：这里的依赖在稍后例子中将被删除 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		
	</dependencies>

</project>
```

编辑根目录的pom.xml，添加auth模块定义
```xml
...
  <modules>
    <module>auth</module>
    <module>gateway</module>
    <module>dept</module>
  </modules>
...
```

### 2.修改配置内容

修改dept的配置内容，如下：
```yaml
server:
  port: 8092

spring:
  application:
    name: dept

  cloud:
    nacos:
      config:
        server-addr: 127.0.0.1:8848
        file-extension: yml
      discovery:
        server-addr: 127.0.0.1:8848
```

### 3.加入@EnableDiscoveryClient

在gate服务的主Application中，加入@EnableDiscoveryClient，同auth服务。

### 4.验证结果

启动gateway服务成功后，在nacos管理界面中，会出现该服务的实例。

![img](./nacos-4.png)

## 二、加入Fiegn组件

### 1.加入Feign的依赖配置

在根目录的pom.xml中加入OpenFeing的包定义。
```xml
...
  <dependencies>
    ...

    <!--服务调用-->
    <dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-openfeign</artifactId>
		</dependency>

  </dependencies>
...
```

### 2.添加@EnableFeignClients

在dept的主Application中，加入以下注解使Feign生效。

```java
# DeptApplicatoin.java
...
@SpringBootApplication
@EnableDiscoveryClient     // 让nacos能发现dept服务
@EnableFeignClients        // 让dept服务可以使用Feign
public class DeptApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeptApplication.class, args);
	}

}
```

### 3.添加接口服务

添加Controller和Service的代码。
```java
# DeptController.java
package cn.riverecho.dept.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.riverecho.dept.api.service.AuthApiService;

@RestController
@RequestMapping("/dept")
public class DeptController {
  
  @Autowired
  private AuthApiService authApiService;

  @GetMapping("/auth")
  public Object auth() {
    return authApiService.auth();
  }
}

# AuthApiService.java
package cn.riverecho.dept.api.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "auth", fallback = AuthApiServiceFallback.class)
public interface AuthApiService {
  
  @GetMapping("/auth")
  Object auth();
}

# AuthApiServiceFallback.java
package cn.riverecho.dept.api.service;

import org.springframework.stereotype.Component;

@Component
public class AuthApiServiceFallback implements AuthApiService {
  
  @Override
  public Object auth() {
    return "Request auth-service /auth failed";
  }
}
```

- @FeignClient标注允许服务的消费者通过声明方式直接调用对应服务，比RestTemplate更简洁和优雅。
- @FeignClient的fallback，指定了当auth服务不可用时的降级服务。


### 4.添加降级服务的配置

对于fallback讲解服务，需要在配置文件中加入以下设置，才可以生效。
```yaml
feign:
  ...
  hystrix:
    enabled: true           # 保证OpenFeign的fallback有效
```

### 5.测试

启动dept服务，输入 http://127.0.0.1:8092/dept/auth ，返回处理结果：
```text
false
```

这个与 http://127.0.0.1:8091/auth 的返回结果是一致的，说明Feign的请求发送处理成功了。
