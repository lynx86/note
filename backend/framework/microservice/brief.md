[原文地址](https://www.zhihu.com/question/289129028/answer/905544634)

# 微服务系统适合的场景

选择微服务与否，跟业务系统的类型和规模，研发、测试、运维的能力，公司发展的阶段等，都有关系。

以下几类系统，比较适合使用微服务架构，或者使用微服务架构改造：

- 大型的前后分离的复杂业务系统后端，业务越复杂，越需要我们合理的设计和划分，长期的维护成本会非常高，历史包袱也很重，这个问题后面会详谈。
- 变化发展特别快的创新业务系统，业务快，就意味着天天要“拥抱变化”，每一块的业务研发都要被业务方压的喘不过气，不做拆分、自动化，一方面研发资源永远不够用，活永远干不完，另一方面不停的在给系统打补丁，越来质量越低，出错的可能性也越来越大。
- 规划中的新大型业务系统，如果有能力一开始就应该考虑做微服务，而不是先做单体，还是发展到一定的阶段再做改造，改造一定是伤筋动骨的大手术，虽然我们可以采取一些策略做得更平滑，但是成本还是比较高的。
- 敏捷自驱的小研发团队，拥抱新技术，可以直接用微服务做系统，不但的通过快速迭代、持续交付，经过一定时间的尝试和调整，形成自己的微服务实践经验，这样在团队扩大时可以把好的经验复制到更大的团队。

反过来，以下几类系统，不太适合一开始就做微服务，

- 小团队，技术基础较薄弱，创业初期或者团队新做的快速原型，这个时候做微服务的收益明显比单体要低，快速把原型做出来怎么方便怎么来，用团队最熟悉的技术栈。
- 流量不高，压力小，业务变化也不大，单体能简单搞定的，就可以先不考虑微服务，不考虑分布式，因为分布式和微服务带来的好处，可能还不足以抵消复杂性增加带来的成本。
- 对延迟很敏感的低延迟高并发系统，低延迟的秘诀就是离 io 能多远就多远，离 cpu 能多近就多近，分布式和微服务，导致增加了网络跳数，延迟就没法降低。
- 技术导向性的系统，技术产品，这类产品跟业务系统不同，常规的业务系统研发方法不是太适合。

# 怎么选择微服务技术栈

Spring cloud和dubbo的关系现在已经不是二选一的关系，融合到了spring cloud alibaba项目里了，可以共存。Spring Cloud 是一个较为成熟的微服务框架，定位为开发人员提供工具，以快速构建分布式系统中的某些常见模式（例如，配置管理，服务发现，断路器，智能路由，微代理，控制总线，一次性令牌，全局锁，Leader 选举，分布式会话，群集状态）。

而 Apache Dubbo 定位是一款高性能、轻量级的开源 Java RPC 框架，它提供了三大核心能力：面向接口的远程方法调用，智能容错和负载均衡，以及服务自动注册和发现。所以，我们可以看到 Dubbo 提供的能力只是 Spring Cloud 的一部分子集。

同时 Dubbo 项目重新维护以后，捐献给了 Apache，项目的导师就是 Spring Cloud 的核心人员。自这时候起 Dubbo 项目就开始在适合 Spring Cloud 体系，结果就是 Alibaba 也基于自己的一系列开源组件和技术，实现了 Spring Cloud Alibaba，并顺利从 [Spring Cloud](https://spring.io/projects/spring-cloud-alibaba) 孵化器毕业。详见：

Spring Cloud Alibaba 致力于提供微服务开发的一站式解决方案。此项目包含开发分布式应用微服务的必需组件，方便开发者通过 Spring Cloud 编程模型轻松使用这些组件来开发分布式应用服务。主要功能和开源技术栈：

- 服务限流降级（Sentinel）：默认支持 WebServlet、WebFlux, OpenFeign、RestTemplate、Spring Cloud Gateway, Zuul, Dubbo 和 RocketMQ 限流降级功能的接入，可以在运行时通过控制台实时修改限流降级规则，还支持查看限流降级 Metrics 监控。
- 服务注册与发现（Nacos）：适配 Spring Cloud 服务注册与发现标准，默认集成了 Ribbon 的支持。
- 分布式配置管理（Nacos）：支持分布式系统中的外部化配置，配置更改时自动刷新。
- 消息驱动能力（Apache RocketMQ）：基于 Spring Cloud Stream 为微服务应用构建消息驱动能力。
- 分布式事务（Seata）：使用 @GlobalTransactional 注解， 高效并且对业务零侵入地解决分布式事务问题。

单纯的讲只考虑Spring Cloud和Dubbo本身的核心功能，我的看法跟题主相反，简单的中小系统用Dubbo，好控制，好上手，非常成熟，也可以自己演化，特别大而复杂的业务系统，用Spring Cloud套件，或者Spring Cloud Alibaba套件。

更详细地说，不局限于Spring Cloud或Dubbo，我们可以选择：

- 服务框架：我们可以选择用 Spring Cloud 或者 Apache Dubbo，包括新兴的 Spring Cloud Alibaba，还有华为贡献的 Apache ServiceComb，蚂蚁金服的 SOFAStack ，Oracle 的 Helidon，Redhat 的 Quarkus，基于 Scala 语言和 Akka 的 Lagom，基于 Grails 语言的 Micronaut，基于 Python 语言的 Nameko，基于 Golang 语言的 go-micro，支持多语言混编的响应式微服务框架 Vert.X，腾讯开源的 Tars，百度开源的 Apache BRPC（孵化中），微博的简化版 Dubbo 框架 Motan 等等。
- 配置中心：Apollo，Nacos，disconf，Spring Cloud Config，或者 Etcd、ZK、Redis 自己封装
- 服务注册发现：Eureka，Consul，或者 Etcd、ZK、Redis 自己封装
- 服务网关：Zuul/Zuul2，Spring Cloud Gateway，nginx 系列的 Open Resty 和 Kong，基于 Golang 的 fagongzi Gateway 等
- 容错限流：Hystrix，Sentinel，Resilience4j，或者直接用 Kong 自带的插件
- 消息处理：Kafka、RabbitMQ、RocketMQ，以及 Pulsar，可以使用 Sping Messaging 或者 Spring Cloud Stream 来简化处理
- 链路监控与日志：开源的链路技术有 CAT、Pinpoint、Skywalking、Zipkin、Jaeger 等，也可以考虑用商业的 APM（比如听云 APM、OneAPM、App Dynamic 等），日志可以用 ELK
- 认证与授权：比如要支持 OAuth2、LDAP，传统的自定义 BRAC 等，可以选择 Spring Security 或者 Apache Shiro 等。。

# 项目规模的衡量、微服务与单体的生产力比较

项目的规模，简单的情况下，可以按照功能或者代码量、开发周期和人月数，甚至成本这些指标来衡量，这些都是客观可度量的，对不同团队来说是有一致的。但是我更加倾向于使用相对值来度量，比如团队的能力来衡量。比如你们团队研发成熟度很高，一个30人月的项目，可能是一个小项目，反过来如果你们团队效率很低，那么一个3人月的项目都是一个很大的项目。项目大小的相对确认标准，需要跟团队能完全hold住的能力挂钩，但是超过团队目前的控制能力边界，就会发生严重混乱。从平均意义上来看：规模越大or研发团队越成熟，微服务的效果越明显。

为什么我们要从单体系统走向微服务架构呢？我们先来看一个图。

![img](brief.png)

这个图 x 轴是系统复杂度，y 轴是开发的生产力，我们可以看到：

- 在系统复杂度很低的时候，单体的生产力要高一点，这是因为拆分微服务，管理这些服务的成本增加了
- 当复杂度开始增加，单体的生产力快速的降低，而微服务则不太受影响，这是因为复杂度大了，单体牵一发而动全身，各种耦合和相互影响太多
- 随着复杂度越来越高，微服务的低耦合开始减低了生产力的衰变，而单体架构的生产力则会降到一个非常低的水平

也就是说微服务应用在复杂度低的情况下，生产力反而比单体系统低。在复杂度高的地方，情况恰恰相反。随着复杂度升高，单体架构的生产力快速下降，而微服务相对平稳，所以，复杂度越高的业务系统，越适合使用微服务架构。