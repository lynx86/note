
## JsonProperty

对于POST或者PUT方法，@RequestBody可以接受请求内容并转化为Java对象。对象中的属性可以通过@JsonProperty标注来给起一个不同的名字。例如：

```java

@Data
@ToString
...
public class User {

  @JsonProperty("name")
  private String nickname;

  ...
}
```

传入的属性名可以是name，但接受的Java对象中的属性名可以是nickname。


## ApiModelProperty

但对于GET方法来讲，如果要将URL中的参数映射为Java对象，需要使用@ModelAttribute注解来自动装配。

```java

public class UserController {

  ...

  @GetMapping("")
  public Object findAllUsers(@ModelAttribute FindUserReq req) {

    ...
  }
}


@Data
@ToString
...
public class FindUserReq {

  @JsonProperty("name")
  private String nickname;

  ...
}

```

这种场合下如果继续使用JsonProperty的话，无法达到预计的效果。GET请求方式下，取得URL中路径参数的处理方法与POST不同，定义了nickname属性的请求会自动被Swagger识别为nickname参数。POST方法中的映射方法，则是直接使用了Jackson模块进行的Json转换。

要让Swagger主动识别参数，需要使用到ApiModelProperty参数，以及手动实现改变量的set方法。

```java

@Data
@ToString
...
public class FindUserReq {

  @ApiModelProperty("name")
  private String name;

  public void setNickname(String nickname) {
    this.name = nickname;
  }

  ...
}
```