
## 重要步骤

1. 安装必要插件
2. 编辑.classpath
3. 打包war文件
4. 运行Tomcat

## 1.安装必要插件

在VSCode上开发JavaWeb项目，基础插件【Java Extension Pack】必不可少。
此插件包括6个插件：

- Language Support for Java(TM) by Red Hat
- Debugger for Java 
- Java Test Runner
- Maven for Java
- Java Dependency Viewer
- Visual Studio IntelliCode

除此之外，对于Java Web项目还需安装插件：

- Tomcat for Java （运行环境）
- Project Manager for Java  (管理Java Web项目)

*注意：插件Project Manager for Java 需要JDK在11及以上，也可不安装。*

## 2.编辑.classpath

对于单一工程的应用来讲，如果是导入的已有文件，classpath应该已经自动配置，不用修改。

如果项目有多个工程以来，如工程A依赖工程B，则项目B的输出需要配置在工程A的.classpath文件中，如：

```xml
...
<!-- B/bin 是项目B的class目录 -->
<classpathentry kind="lib" path="../B/bin"/>
...
```

如此设置，项目A的编译才会正常。

由于在下一步打包war文件中，需要使用到项目B，需要手动将项目B打成jar包，并添加到项目A的WebContent/WEB-INF/lib目录中。（用于下一步打war包）


## 3.打包war文件


插件【Tomcat for Java]有命令可打包Java项目，但只能选择对已打开目录进行打包。对于Java Web项目来说，应该对部署工程的WebContent进行打包。所以目前只能使用jar命令来手动打包。

```bash
# cd projectA/WebContent
# jar -cvf myApp.war .

# // 经测试发现，进入WebContent并且对本目录(.)打包是唯一能正常运行的命令方式
# // 其它打包如：
# //   jar -cvf myApp.war WebContent    (WebContent目录上层)
# //   jar -cvf myApp.war ./WebContent  (WebContent目录上层)
# //   jar -cvf myApp.war WebContent/*  (WebContent目录上层)
# //   jar -cvf myApp.war ./*  (WebContent目录内层)
# // 均不能被Tomcat正常解压和运行
```

## 4.运行Tomcat

在TOMCAT SERVERS的面板上点击+，选择已安装的Tomcat路径，确认后即可添加Tomcat实例。

之后，在已打包好的war上右键->[Run On Tomcat Server]，项目顺利启动！

## 5.后记

### 问题

传统Java Web项目与SpringBoot项目不同，没有Application的入口main方法，打war包并放入Tomcat后是如何自动执行的？

### 任务

空闲时写一个VSCode插件，自动编译依赖项目到主项目，并一键部署到Tomcat服务中。