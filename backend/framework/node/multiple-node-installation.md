
本指南应用于Mac系统，Linux系统同样适用。

## 1.安装Node

一般会找[Node官网](https://nodejs.org/)，但网速太慢，推荐在[淘宝镜像](https://npm.taobao.org/mirrors/node)直接下载。

安装完成后，yarn和npm包应该也同样安装完成，可通过命令行确认。
```bash
# node -v
v12.17.0

# yarn -v
1.22.4
```

## 2.设置yarn的国内镜像

一般可以直接修改为淘宝镜像

```bash
# yarn config set registry https://registry.npm.taobao.org/
```

如果想在多个镜像源间切换，可以使用yrm模块

```bash
# yarn global add yrm

# yrm ls  // 列出所有镜像源
  npm -----  https://registry.npmjs.org/
  cnpm ----  http://r.cnpmjs.org/
  taobao --  https://registry.npm.taobao.org/
  nj ------  https://registry.nodejitsu.com/
  rednpm -- http://registry.mirror.cqupt.edu.cn
  skimdb -- https://skimdb.npmjs.com/registry
  yarn ----  https://registry.yarnpkg.com

# yrm use taobao  // 使用淘宝源

# yrm test taobao  // 测试镜像速度

```

## 3.使用n模块安装多个node版本

```bash
# yarn global add n
```

安装好以后，可以使用n stable命令来安装最新稳定版的node环境。但n模块默认使用 https://nodejs.org 作为安装包来源，可以指定为国内镜像。

```bash

# export N_NODE_MIRROR=https://npm.taobao.org/mirrors/node
# sudo -E n 12.0.0
```

注意这里的-E参数，是必须的。因为sudo命令会切换到su用户来执行n命令，必须指定之前的export效果继续生效才可以使用淘宝源。

-E的说明如下：

> -E, --preserve-env
Indicates to the security policy that the user wishes to preserve their existing
environment variables. The security policy may return an error if the user does
not have permission to preserve the environment.

安装好新版本的node后，可以通过n命令来选择需要用到的版本。

```bash
# n

node/12.0.0
node/12.17.0

Use up/down arrow keys to select a version, return key to install, d to delete, q to quit

# node -v
v12.0.0

```