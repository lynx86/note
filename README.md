* 前端
  * [基础知识](/frontend/basic/)
  * [框架学习](/frontend/framework/)
  * [排错记录](/frontend/note/)
* 后端
  * [基础知识](/backend/basic/)
  * [框架学习](/backend/framework/)
  * [排错记录](/backend/note/)
* 运维
  * [数据库](devops/db/)
  * [操作系统](/devops/os/)
  * [运维工具](/devops/tool/)